package com.ivanabramov.sqr;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;

public class TestYoutube {
    WebDriver driver;
    
    // Setup driver
    @BeforeClass
    public void initDriver() throws MalformedURLException {
        FirefoxOptions options = new FirefoxOptions();
        options.addArguments("--headless");
        String webdriverHost = System.getenv("WEBDRIVER_HOST");

        if (webdriverHost == null || webdriverHost.isEmpty()) {
            driver = new FirefoxDriver(options);
        } else {
            driver = new RemoteWebDriver(new URL(webdriverHost), options);
        }
        driver.manage().timeouts().implicitlyWait(10000, TimeUnit.MILLISECONDS);
    }

    @Test
    public void testVideoScrolling(){
        driver.get("https://www.youtube.com");

        System.out.println("Page Title is " + driver.getTitle());

        int numberOfVideosBefore = driver.findElements(By.xpath("//div[@class='style-scope ytd-rich-grid-row']")).size();
        System.out.println("Number of videos beforee scroll: " +  numberOfVideosBefore);

        Assert.assertTrue(numberOfVideosBefore > 1);

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500000)");

        int numberOfVideosAfter = driver.findElements(By.xpath("//div[@class='style-scope ytd-rich-grid-row']")).size();
        System.out.println("Number of videos after scroll: " +  numberOfVideosAfter);
        
        Assert.assertTrue(numberOfVideosAfter > 1);

        Assert.assertTrue(numberOfVideosBefore < numberOfVideosAfter);

        driver.quit();
    }
}
